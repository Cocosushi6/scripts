#!/usr/bin/env perl
use strict;
use warnings;
use JSON;
use Data::Dumper;

sub get_json_tasks {
	my %tasksWithDate;
	my $jsonData = $_[0];
	my $rawRef = decode_json $jsonData;
	my $i = 0;
	foreach my $task (@$rawRef) {
		my $id = $task->{'id'};
		my $dueDate = $task->{'due'};
		my $description = $task->{'description'};
		if(defined $dueDate && defined $description && defined $id) {
			$tasksWithDate{$id} = [$dueDate, $description];
			$i++;
		}
	}	
	print $i . "; ";
	return %tasksWithDate;
}


sub send_notif {
	my $text = $_[0];
	system("/usr/bin/notify-send --urgency=critical 'Tasks' '$text'"); 
}

my $hourOffset = $ARGV[0]; # between 

if($hourOffset < -12 && $hourOffset > 14) {
	$hourOffset = 0;
}

my $jsonTaskData = `/usr/bin/task export status:pending`;
my %rawTaskData = get_json_tasks($jsonTaskData);
my $numTasksLate = 0;
my @todayTasks;
my ($csec,$cmin,$chour,$cmday,$cmon,$cyear,$cwday,$cyday,$isdst) = localtime();		# c for current 
$cmon++;
$cyear += 1900;
foreach my $value (values %rawTaskData) {
	#${$value}[0] is the due date
	#task time
	my $year = substr ${$value}[0], 0, 4;
	my $month = substr ${$value}[0], 4, 2;
	my $day = substr ${$value}[0], 6, 2;
	my $hour = substr ${$value}[0], 9, 2;
	my $minute = substr ${$value}[0], 11, 2;

	$hour += $hourOffset;

	#task time
	print $year . ", " . $month . ", " . $day . ", " . $hour . ", " . $minute . " ; \n";

	if($year < $cyear) {
		$numTasksLate++;
	} elsif ($year == $cyear) {
		if($month < $cmon) {
			$numTasksLate++;
		} elsif($month == $cmon) {
			if($day < $cmday) {
				$numTasksLate++;
			} elsif($day == $cmday) {
				push(@todayTasks, ${$value}[1]);
				if($hour < $chour) {
					$numTasksLate++;
				} elsif($hour == $chour) {
					if($minute < $cmin) {
						$numTasksLate++;
						send_notif("You must do '${value}[1]' now !");
					} elsif($minute == $cmin || $minute < $cmin+10) {
						send_notif("Task '${$value}[1]' happens now !");
					}
				}	
			}
		}
	}
}
my $todayTasksSize = @todayTasks;
if($todayTasksSize > 0) {
my $notifText = "You have $todayTasksSize tasks to do today, these are : \n"; 
foreach my $text (@todayTasks) {
	$notifText .= $text;
}
send_notif($notifText);
}
