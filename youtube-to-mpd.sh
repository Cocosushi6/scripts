#!/bin/bash
#$1 is url for the file, $2 is MPD's music folder


youtube-dl -o "/tmp/$1.%(ext)s" "https://www.youtube.com/watch?v=$1" 
file=`ls /tmp | grep $1`
mv /tmp/$file $2
mpc update 
